package com.imamfarisi.model;

/*
author imam-pc 
created on 20/04/2020
*/

public class Universitas {
    private Long id;
    private String nama;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "Id :" + id + ", Nama : " + nama;
    }
}
