package com.imamfarisi;

/* 
author imam-pc 
created on 20/04/2020
*/

import com.imamfarisi.dao.UniversitasDAO;
import com.imamfarisi.model.Universitas;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("main.xml");

        Universitas univ = context.getBean("universitas", Universitas.class);
        //UniversitasDAO univDao = context.getBean("univJdbcDao", UniversitasDAO.class);
        UniversitasDAO univDao = context.getBean("univJdbcTemplateDao", UniversitasDAO.class);

        univDao.deleteAllData();
        univDao.insertData(univ);
        univDao.updateData(univ);

        List<Universitas> listUniv = univDao.getAllData();
        listUniv.forEach(System.out::println);//method reference java 8+

    }
}
