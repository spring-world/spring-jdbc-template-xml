package com.imamfarisi.dao;

/* 
author imam-pc 
created on 20/04/2020
*/

import com.imamfarisi.model.Universitas;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UniversitasJDBCDaoImpl implements UniversitasDAO {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void insertData(Universitas data) {
        try {
            Connection conn = dataSource.getConnection();
            String sql = " INSERT INTO universitas(id, nama) VALUES (?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, data.getId());
            ps.setString(2, data.getNama());
            ps.execute();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateData(Universitas data) {
        try {
            Connection conn = dataSource.getConnection();
            String sql = " UPDATE universitas SET nama = ? WHERE id = ? ";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, data.getNama());
            ps.setLong(2, data.getId());
            ps.execute();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Universitas> getAllData() {
        List<Universitas> listUniv = new ArrayList<>();
        try {
            Connection conn = dataSource.getConnection();
            String sql = " SELECT * FROM universitas ";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Universitas data = new Universitas();
                data.setId(rs.getLong("id"));
                data.setNama(rs.getString("nama"));
                listUniv.add(data);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listUniv;
    }

    public void deleteAllData() {
        try {
            Connection conn = dataSource.getConnection();
            String sql = " DELETE FROM universitas ";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.execute();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
