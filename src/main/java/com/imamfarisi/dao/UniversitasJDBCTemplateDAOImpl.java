package com.imamfarisi.dao;

/* 
author imam-pc 
created on 20/04/2020
*/

import com.imamfarisi.model.Universitas;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class UniversitasJDBCTemplateDAOImpl implements UniversitasDAO {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertData(Universitas data) {
        String sql = " INSERT INTO universitas(id, nama) VALUES (?, ?)";
        jdbcTemplate.update(sql, data.getId(), data.getNama());
    }

    @Override
    public void updateData(Universitas data) {
        String sql = " UPDATE universitas SET nama = ? WHERE id = ? ";
        jdbcTemplate.update(sql, data.getNama(), data.getId());
    }

    @Override
    public List<Universitas> getAllData() {
        String sql = " SELECT * FROM universitas ";
        List<Universitas> listUniv = jdbcTemplate.query(sql, (rs, row) -> {
            Universitas univ = new Universitas();
            univ.setId(rs.getLong("id"));
            univ.setNama(rs.getString("nama"));
            return univ;

        });
        return listUniv;
    }

    @Override
    public void deleteAllData() {
        String sql = " DELETE FROM universitas ";
        jdbcTemplate.update(sql);
    }
}
